from matplotlib import pyplot
import numpy as np
from graph.random import er_graph
from performance import execution_time
from graph.utils import in_deg_distribuition


def main():

    p_values = [0.2, 0.4, 0.6, 0.8]
    graphs_nodes = [1000] * len(p_values)

    for i in range(len(p_values)):
        plot = pyplot.subplot(np.math.ceil(len(p_values) / 2), 2, i + 1)
        print("Generating graph {}".format(i))
        graph = er_graph(graphs_nodes[i], p_values[i], 'matrix')
        print("Computing distribution {}".format(i))
        distribution = execution_time(in_deg_distribuition)(graph)

        print("Plotting {}".format(i))
        plot.plot(range(len(distribution)), distribution)
        plot.grid(True)
        plot.set_title("ER with p: {0}".format(p_values[i]))

    pyplot.show()


if __name__ == "__main__":
    main()
