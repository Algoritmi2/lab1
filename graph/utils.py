from typing import List

from graph import Graph


def in_deg_distribuition(graph: Graph) -> List[float]:

    indeg_occ_map = dict()
    for v in graph:
        in_deg = graph.in_degree(v)
        if in_deg in indeg_occ_map:
            indeg_occ_map[in_deg] += 1
        else:
            indeg_occ_map[in_deg] = 1

    values = [0] * (graph.get_nodes_count())
    for i in range(graph.get_nodes_count()):
        if i in indeg_occ_map:
            values[i] = indeg_occ_map[i]/graph.get_nodes_count()

    return values
