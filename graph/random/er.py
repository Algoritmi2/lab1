import numpy as np
from graph import MatrixGraph, ListGraph, Graph


def er_graph(n: int, p: float, representation: str = 'list') -> Graph:
    """
    Creates a random graph using the ER algorithm.
    :param n: The number of nodes in the graph.
    :param p: The probability that 2 nodes are connected with a an edge, must be in [0, 1)
    :param representation: The internal representation of the generated graph,
                           `list` to use an adjacencies list, `matrix` to use adjacencies matrix.
    :return:
    """
    if p >= 1 or p < 0:
        raise ValueError("Unsupported value for the `p` param, the value must be in [0,1)")

    graph = None
    if representation == 'list':
        graph = ListGraph(n, Graph.Type.ORIENTED)
    elif representation == 'matrix':
        graph = MatrixGraph(n, Graph.Type.ORIENTED)
    else:
        raise ValueError("Unsupported value: {} for the representation param.".format(representation))

    for i in range(n):
        graph.add_node()

    for u in range(graph.get_nodes_count()):
        for v in range(graph.get_nodes_count()):
            if v == u:
                continue
            node_u = graph.get_node_by_index(u)
            node_v = graph.get_node_by_index(v)
            a = np.random.sample()
            if a < p:
                graph.add_edge(node_u, node_v)
    return graph
