from typing import List

from graph.Graph import Graph, Node


class MatrixGraph(Graph):

    """
    Implementation of the graph interface, that represents a graph using an adjacencies matrix.
    """

    def __init__(self, nodes_capacity: int, graph_type: Graph.Type = Graph.Type.ORIENTED):
        """
        :param nodes_capacity: The initial nodes capacity of the graph.
        :param graph_type: The graph type, can be Oriented(Type.ORIENTED) or Not oriented(Type.NOT_ORIENTED).
        """
        self._nodes_count = 0
        self._type = graph_type
        self._in_degree_list = [0] * nodes_capacity
        self._out_degree_list = [0] * nodes_capacity
        self._nodes = [None] * nodes_capacity  # type: List[Node]
        if graph_type == Graph.Type.ORIENTED:
            self._adjacencies_matrix = [[0 for x in range(nodes_capacity)] for y in range(nodes_capacity)]
        else:
            self._adjacencies_matrix = [None] * nodes_capacity
            for i in range(nodes_capacity):
                self._adjacencies_matrix[i] = [0] * i

    def add_node(self, value: object = None):
        v = Node(self._nodes_count, value)
        if v.index > len(self._nodes):
            self._nodes.append(v)
            self._in_degree_list.append(0)
            self._out_degree_list.append(0)
            self._adjacencies_matrix.append([0] * (self._nodes_count + 1))
            for i in range(self._nodes_count):
                self._adjacencies_matrix[i].append(0)
        else:
            self._nodes[v.index] = v
        self._nodes_count += 1
        return v

    def get_node_by_index(self, index: int) -> Node:
        return self._nodes[index]

    def get_nodes(self) -> List[Node]:
        return self._nodes[:self._nodes_count]

    def get_nodes_count(self) -> int:
        return self._nodes_count

    def out_degree(self, v: Node) -> int:
        return self._out_degree_list[v.index]

    def in_degree(self, v: Node) -> int:
        return self._in_degree_list[v.index]

    def add_edge(self, tail: Node, head: Node):
        self._in_degree_list[head.index] += 1
        self._out_degree_list[tail.index] += 1
        if self._type == Graph.Type.NOT_ORIENTED:
            self._in_degree_list[tail.index] += 1
            self._out_degree_list[head.index] += 1
            first, second = self.__sort_nodes(head, tail)
            self._adjacencies_matrix[first.index][second.index] += 1
        else:
            self._adjacencies_matrix[tail.index][head.index] += 1
        return self

    def are_connected(self, v: Node, u: Node) -> bool:
        first, second = self.__sort_nodes(v, u)
        return self._adjacencies_matrix[first.index][second.index] > 0

    def __sort_nodes(self, v: Node, u: Node):
        return (v, u) if self._type == Graph.Type.ORIENTED or v.index >= u.index else (u, v)
