from typing import List

from graph.Graph import Graph, Node


class ListGraph(Graph):

    """
    Implementation of the graph interface, that represents a graph using an adjacencies list.
    """

    def __init__(self, nodes_capacity: int, graph_type: Graph.Type = Graph.Type.ORIENTED):
        """
        :param nodes_capacity: The initial nodes capacity of the graph.
        :param graph_type: The graph type, can be Oriented(Type.ORIENTED) or Not oriented(Type.NOT_ORIENTED).
        """
        self._nodes_count = 0
        self._type = graph_type
        self._in_degree_list = [0] * nodes_capacity
        self._nodes = [None] * nodes_capacity  # type: List[Node]
        self._adjacencies_list = [[] for x in range(nodes_capacity)]

    def add_node(self, value: object = None):
        v = Node(self._nodes_count, value)
        if v.index > len(self._nodes):
            self._nodes.append(v)
            self._adjacencies_list.append([])
            self._in_degree_list.append(0)
        else:
            self._nodes[v.index] = v
        self._nodes_count += 1
        return v

    def get_node_by_index(self, index: int) -> Node:
        return self._nodes[index]

    def get_nodes(self) -> List[Node]:
        return self._nodes[:self._nodes_count]

    def get_nodes_count(self) -> int:
        return self._nodes_count

    def out_degree(self, v: Node) -> int:
        return len(self._adjacencies_list[v.index])

    def in_degree(self, v: Node) -> int:
        return self._in_degree_list[v.index]

    def add_edge(self, tail: Node, head: Node):
        self._adjacencies_list[tail.index].append(self._nodes[head.index])
        self._in_degree_list[head.index] += 1
        if self._type == Graph.Type.NOT_ORIENTED:
            self._adjacencies_list[head.index].append(self._nodes[tail.index])
            self._in_degree_list[tail.index] += 1
        return self

    def are_connected(self, v: Node, u: Node) -> bool:
        connected = False
        adjacencies_nodes = self._adjacencies_list[v.index]
        if adjacencies_nodes:
            for i in range(len(adjacencies_nodes)):
                if adjacencies_nodes[i].index == u.index:
                    connected = True
                    break
        return connected

