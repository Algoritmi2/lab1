from matplotlib import pyplot

from graph import Graph, create_graph
from graph.utils import in_deg_distribuition
from performance import execution_time


@execution_time
def parse_graph(dataset_path: str) -> Graph:
    graph = create_graph(27770, 352807)
    node_map = dict()
    with open(dataset_path) as file:
        for line in file.readlines():
            if line[0] == '#':
                continue
            nums = line[:-1].split('\t')
            if len(nums) != 2:
                raise Exception("Unsupported input format.")
            tail_pubblication = nums[0]
            head_publication = nums[1]

            tail_node = node_map.get(tail_pubblication)  # O(n) n = number of nodes in data set.
            if tail_node is None:
                tail_node = graph.add_node(tail_pubblication)
                node_map[tail_pubblication] = tail_node

            head_node = node_map.get(head_publication)
            if head_node is None:
                head_node = graph.add_node(head_publication)
                node_map[head_publication] = head_node
            graph.add_edge(tail_node, head_node)
    return graph


def main():
    graph = parse_graph('dataset.txt')

    distribution = in_deg_distribuition(graph)

    out_deg = 0
    for v in graph:
        out_deg += graph.out_degree(v)
    print("Out deg av: {}".format(out_deg/graph.get_nodes_count()))

    pyplot.scatter(range(len(distribution)), distribution, s=1)
    pyplot.grid(True)
    pyplot.xscale('log')
    pyplot.yscale('log')
    pyplot.show()

if __name__ == "__main__":
    main()
