from matplotlib import pyplot

from graph.random import dpa_graph
from graph.utils import in_deg_distribuition
from performance import execution_time


@execution_time
def get_xy():
    n = 27770
    m = 12
    graph = dpa_graph(n, m)

    y = in_deg_distribuition(graph)
    x = range(0, len(y))
    return x, y


def main():
    x, y = get_xy()
    pyplot.scatter(x, y, s=1)
    pyplot.grid(True)
    pyplot.xscale('log')
    pyplot.yscale('log')
    pyplot.show()

if __name__ == '__main__':
    main()
