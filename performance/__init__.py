import time


# Copied from https://stackoverflow.com/questions/15707056/get-time-of-execution-of-a-block-of-code-in-python-2-7
def execution_time(func):
    def wrapper(*args, **kwargs):
        beg_ts = time.time()
        retval = func(*args, **kwargs)
        end_ts = time.time()
        print("{0} - Execution time: {1} sec".format(func.__name__, (end_ts - beg_ts)))
        return retval
    return wrapper